<?php
namespace App\Core;

use PDO;
use App\Post\PostsRepository;
use App\Post\PostsController;

class Container
{
    private $recipes = [];
    private $instances = [];

    //Konstruktor
    public function __construct()
    {
        $this->recipes = [
            'postsController' => function() {
                return new PostsController(
                    $this->make('postsRepository')
                );
            },
            'postsRepository' => function() {

                return new postsRepository(
                    $this->make("pdo")
                );
            },
            'pdo' => function() {
                $pdo = new PDO(
                    'mysql:host=localhost;dbname=blog;charset=utf8',
                    'root',
                    ''
                );
                $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
                return $pdo;
            }
        ];
    }

    //Make-Methode
    public function make($name)
    {
        if(!empty($this->instances[$name]))
        {
            return $this->instances[§name];
        }

        if (isset($this->recipes[$name])) {
            $this->instances[$name] = $this->recipes[$name]();
        }

        return $this->instances[$name];
    }

}
?>